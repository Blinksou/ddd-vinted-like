# Parameters
HTTP_PORT     = 8000

# Executables
EXEC_PHP      = php
COMPOSER      = composer
GIT           = git
YARN          = yarn

# Alias
SYMFONY       = $(EXEC_PHP) bin/console


## —— Project ————————————————————————————————————————————————————————————
install: composer.lock ## Install vendors according to the current composer.lock file
	@$(COMPOSER) install --no-progress --prefer-dist --optimize-autoloader
	@$(SYMFONY) d:d:c
	@$(SYMFONY) d:s:u -f

user:
	@$(SYMFONY) app:make:user

admin-user:
	@$(SYMFONY) app:make:user --as-admin

## —— Symfony ———————————————————————————————————————————————————————————————
cc:
	@$(SYMFONY) cache:clear

warmup:
	@$(SYMFONY) cache:warmup

purge: ## Purge cache and logs
	@rm -rf var/cache/* var/logs/*

serve:
	@$(SYMFONY_BIN) serve --no-tls --daemon --port=$(HTTP_PORT)

unserve: ## Stop the webserver
	@$(SYMFONY_BIN) server:stop

## —— Docker ————————————————————————————————————————————————————————————————
up:
	$(DOCKER_COMP) up --detach

build:
	$(DOCKER_COMP) build --pull --no-cache

down:
	$(DOCKER_COMP) down --remove-orphans

## —— Yarn / JavaScript —————————————————————————————————————————————————————
dev: ## Rebuild assets for the dev env
	@$(YARN) install --check-files
	@$(YARN) run encore dev

watch: ## Watch files and build assets when needed for the dev env
	@$(YARN) run encore dev --watch

prod: ## Build assets for production
	@$(YARN) run encore production
