<?php

declare(strict_types=1);

namespace App\Infrastructure\Image\Processor;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;

final class ImageUploadProcessor
{
    public function __construct(
        private readonly SluggerInterface      $slugger,
        private readonly ParameterBagInterface $parameterBag
    )
    {
    }

    public function process(FormInterface $form, Request $request, string $collectionFieldName = 'images', string $imageFieldName = 'image'): array
    {
        $processedImages = [];

        foreach ($form->get($collectionFieldName)->getData() as $k => $image) {
            $file = $request->files->get($form->getName())[$collectionFieldName][$k][$imageFieldName];

            if (!$file instanceof UploadedFile) {
                continue;
            }

            $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            // this is needed to safely include the file name as part of the URL
            $safeFilename = $this->slugger->slug(strtolower($originalFilename));
            $newFilename = $safeFilename . '-' . uniqid('', true) . '.' . $file->guessExtension();

            // Move the file to the directory where uploads are stored
            $file->move(
                $this->parameterBag->get('uploads_directory'),
                $newFilename
            );

            $processedImages[] = $newFilename;
        }

        return $processedImages;
    }
}
