<?php

declare(strict_types=1);

namespace App\Infrastructure\Mailing;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

final class Mailer
{
    public function __construct(
        private readonly Environment     $twig,
        private readonly MailerInterface $mailer,
    )
    {
    }

    public function createEmail(string $template, array $data = []): Email
    {
        $this->twig->addGlobal('format', 'html');
        $html = $this->twig->render($template, array_merge($data, ['layout' => 'mails/base.html.twig']));
        $this->twig->addGlobal('format', 'text');
        $text = $this->twig->render($template, array_merge($data, ['layout' => 'mails/base.text.twig']));

        return (new Email())
            ->from('noreply@vinted.com')
            ->html($html)
            ->text($text);
    }

    public function send(Email $email): void
    {
        $this->mailer->send($email);
    }

    public function sendAsync(Email $email): void
    {
        throw new \RuntimeException('Not implemented yet');
    }
}
