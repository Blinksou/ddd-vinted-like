<?php

declare(strict_types=1);

namespace App\Infrastructure\Mailing;

use App\Domain\Auth\Event\UserCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class AuthSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly Mailer $mailer,
    )
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            UserCreatedEvent::class => 'onRegister',
        ];
    }

    public function onRegister(UserCreatedEvent $event): void
    {
        $email = $this->mailer->createEmail(
            'mails/auth/register.html.twig',
            [
                'user' => $event->getUser(),
            ])
            ->to($event->getUser()->getEmail())
            ->subject('Vinted | Confirmation du compte');

        $this->mailer->send($email);
    }
}
