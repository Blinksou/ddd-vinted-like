<?php

declare(strict_types=1);

namespace App\Domain\Auth\Event;

use App\Domain\Auth\Entity\User;

final class UserCreatedEvent
{
    public function __construct(
        private readonly User $user
    )
    {
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
