<?php

declare(strict_types=1);

namespace App\Domain\Auth\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException;

final class UserNotFoundException extends AuthenticationException
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getMessageKey(): string
    {
        return 'User not found.';
    }
}
