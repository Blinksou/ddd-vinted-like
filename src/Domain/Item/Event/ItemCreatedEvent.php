<?php

declare(strict_types=1);

namespace App\Domain\Item\Event;

use App\Domain\Item\Entity\Item;

final class ItemCreatedEvent
{
    public function __construct(
        private readonly Item $item
    )
    {
    }

    public function getItem(): Item
    {
        return $this->item;
    }
}
