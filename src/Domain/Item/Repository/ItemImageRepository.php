<?php

declare(strict_types=1);

namespace App\Domain\Item\Repository;

use App\Domain\Item\Entity\ItemImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ItemImage>
 *
 * @method ItemImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method ItemImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method ItemImage[]    findAll()
 * @method ItemImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ItemImage::class);
    }

    public function add(ItemImage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ItemImage $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
