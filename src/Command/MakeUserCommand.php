<?php

declare(strict_types=1);

namespace App\Command;

use App\Domain\Auth\Entity\User;
use App\Infrastructure\Enum\Auth\UserRole;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:make:user',
    description: 'Create an user',
)]
final class MakeUserCommand extends Command
{
    private const AS_ADMIN = 'as-admin';

    public function __construct(
        private readonly UserPasswordHasherInterface $userPasswordHasher,
        private readonly EntityManagerInterface      $manager
    )
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Create a new user')
            ->addOption('email', null, InputOption::VALUE_REQUIRED, 'Email of the user')
            ->addOption('firstname', null, InputOption::VALUE_REQUIRED, 'First name of the user')
            ->addOption('lastname', null, InputOption::VALUE_REQUIRED, 'Last name of the user')
            ->addOption('password', null, InputOption::VALUE_REQUIRED, 'Password')
            ->addOption(self::AS_ADMIN, null, InputOption::VALUE_NONE);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');

        // Email
        $email = $input->getOption('email');
        if (!$email) {
            $question = new Question('Enter the email: ');
            $email = $helper->ask($input, $output, $question);
            if (!$email) {
                $io->error("Email must not be empty");
                return Command::INVALID;
            }
        }

        // Ask password
        $password = $input->getOption('password');
        if (!$password) {
            $question = new Question('Enter the password: ');
            $question->setHidden(true);
            $password = $helper->ask($input, $output, $question);
            if (!$password) {
                $io->error("Password must not be empty");
                return Command::INVALID;
            }
        }

        // Create the user
        $user = new User();

        $user
            ->setEmail($email)
            ->setPassword($this->userPasswordHasher->hashPassword($user, $password));

        if ($input->getOption(self::AS_ADMIN)) {
            $user->setRoles([UserRole::ADMIN]);
        }

        // Flush it ! :)
        $this->manager->persist($user);
        $this->manager->flush();

        $io->success(sprintf(
            'User %s created :)',
            $user->getEmail()
        ));

        return Command::SUCCESS;
    }
}
