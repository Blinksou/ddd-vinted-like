<?php

declare(strict_types=1);

namespace App\Http\Form;

use App\Domain\Item\Entity\Item;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ItemFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $item = $builder->getData();

        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom',
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
            ])
            ->add('price', NumberType::class, [
                'label' => 'Prix',
                'data' => 1
            ])
            ->add('images', CollectionType::class, [
                'mapped' => false,
                'entry_type' => ItemImageFormType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'data' => null !== $item->getId() ? $item->getImages() : new ArrayCollection(),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Item::class,
            'attr' => [
                'novalidate' => 'novalidate',
            ],
        ]);
    }
}
