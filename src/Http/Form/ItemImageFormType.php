<?php

declare(strict_types=1);

namespace App\Http\Form;

use App\Domain\Item\Entity\ItemImage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Dropzone\Form\DropzoneType;

final class ItemImageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fileName', HiddenType::class, ['required' => false])
            ->add('image', DropzoneType::class, [
                'mapped' => false,
                'by_reference' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => ItemImage::class,
                'block_prefix' => 'item_image',
            ]);
    }
}
