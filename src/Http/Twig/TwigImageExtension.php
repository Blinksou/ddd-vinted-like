<?php

declare(strict_types=1);

namespace App\Http\Twig;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class TwigImageExtension extends AbstractExtension
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag
    )
    {
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('vinted_item_image_asset', [$this, 'getItemImagePath']),
        ];
    }

    public function getItemImagePath(string $fileName): string
    {
        return sprintf('/%s/%s', $this->getUploadDirectoryFormatted('uploads_directory'), $fileName);
    }

    private function getUploadDirectoryFormatted(string $parameterKey): string
    {
        $uploadDirectory = $this->parameterBag->get($parameterKey);

        $explodedUploadDirectory = explode('/', $uploadDirectory);

        // last element is the directory
        return array_pop($explodedUploadDirectory);
    }
}
