<?php

declare(strict_types=1);

namespace App\Http\Controller\Item;

use App\Domain\Item\Entity\Item;
use App\Domain\Item\Entity\ItemImage;
use App\Domain\Item\Event\ItemCreatedEvent;
use App\Domain\Item\Repository\ItemRepository;
use App\Http\Controller\AbstractController;
use App\Http\Form\ItemFormType;
use App\Infrastructure\Image\Processor\ImageUploadProcessor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ItemController extends AbstractController
{
    public function __construct(
        private readonly ItemRepository       $itemRepository,
        private readonly ImageUploadProcessor $imageUploadProcessor
    )
    {
    }

    #[Route('/item/create', name: 'item_create')]
    public function create(Request $request): Response
    {
        $item = new Item();
        $form = $this->createForm(ItemFormType::class, $item)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $processedImages = $this->imageUploadProcessor->process($form, $request);
            foreach ($processedImages as $image) {
                $itemImage = (new ItemImage())->setFileName($image);

                $item->addImage($itemImage);
            }

            $this->itemRepository->add($item, true);

            $this->addSuccessFlash('Item créé!');

            $this->dispatcher->dispatch(new ItemCreatedEvent($item));

            return $this->redirectToRoute('home');
        }

        return $this->renderForm('pages/item/create.html.twig', compact('form'));
    }

    #[Route('/item/{id}/edit', name: 'item_edit')]
    public function edit(Request $request, Item $item): Response
    {
        $form = $this->createForm(ItemFormType::class, $item)->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $processedImages = $this->imageUploadProcessor->process($form, $request);
            foreach ($processedImages as $image) {
                $itemImage = (new ItemImage())->setFileName($image);

                $item->addImage($itemImage);
            }

            $this->itemRepository->flush();

            $this->addSuccessFlash('Item modifié!');

            return $this->redirectToRoute('home');
        }

        return $this->renderForm('pages/item/create.html.twig', compact('form'));
    }
}
