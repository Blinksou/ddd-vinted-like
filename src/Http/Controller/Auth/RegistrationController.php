<?php

declare(strict_types=1);

namespace App\Http\Controller\Auth;

use App\Domain\Auth\Entity\User;
use App\Domain\Auth\Event\UserCreatedEvent;
use App\Http\Controller\AbstractController;
use App\Http\Form\RegistrationFormType;
use App\Infrastructure\Security\TokenGeneratorService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

final class RegistrationController extends AbstractController
{
    #[Route('/inscription', name: 'auth_register')]
    public function register(
        Request                     $request,
        UserPasswordHasherInterface $userPasswordHasher,
        EntityManagerInterface      $entityManager,
        TokenGeneratorService       $tokenGenerator,
    ): Response
    {
        $user = new User();
        $form = $this
            ->createForm(RegistrationFormType::class, $user)
            ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setConfirmationToken($tokenGenerator->generate(60));

            $entityManager->persist($user);
            $entityManager->flush();

            $this->dispatcher->dispatch(new UserCreatedEvent($user));

            return $this->redirectToRoute('auth_login');
        }

        return $this->render('pages/auth/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/inscription/confirmation/{id<\d+>}', name: 'auth_register_confirm')]
    public function confirmToken(
        User                   $user,
        Request                $request,
        EntityManagerInterface $em
    ): RedirectResponse
    {
        $token = $request->get('token');
        if (empty($token) || $token !== $user->getConfirmationToken()) {
            $this->addErrorFlash("Ce token n'est pas valide");

            return $this->redirectToRoute('auth_register');
        }

        $user->setConfirmationToken(null);
        $em->flush();
        $this->addSuccessFlash('Votre compte a été validé.');

        return $this->redirectToRoute('auth_login');
    }
}
