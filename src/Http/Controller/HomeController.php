<?php

declare(strict_types=1);

namespace App\Http\Controller;

use App\Domain\Item\Repository\ItemRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class HomeController extends AbstractController
{
    public function __construct(
        private readonly ItemRepository $itemRepository
    )
    {
    }

    #[Route('/', name: 'home')]
    public function index(): Response
    {
        $items = $this->itemRepository->findAll();
        
        return $this->render('pages/home/index.html.twig', compact('items'));
    }
}
