<?php

declare(strict_types=1);

namespace App\Http\Controller\Admin;

use App\Domain\Item\Entity\ItemCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

final class ItemCategoryCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ItemCategory::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW);
    }

    public function configureFields(string $pageName): iterable
    {
        yield TextField::new('name');

        yield AssociationField::new('items')
            ->setCrudController(ItemCrudController::class)
            ->autocomplete();
    }
}
