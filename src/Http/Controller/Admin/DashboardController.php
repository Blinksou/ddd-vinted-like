<?php

declare(strict_types=1);

namespace App\Http\Controller\Admin;

use App\Domain\Auth\Entity\User;
use App\Domain\Item\Entity\Item;
use App\Domain\Item\Entity\ItemCategory;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return $this->render('pages/admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Ddd Vinted');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::section('Gestion des utilisateurs');
        yield MenuItem::linkToCrud('Liste des inscrits', 'fas fa-users', User::class);

        yield MenuItem::section('Gestion des items');

        yield MenuItem::subMenu('Items', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud('Créer un Item', 'fas fa-plus-circle', Item::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Voir les Items', 'fas fa-eye', Item::class),
        ]);

        yield MenuItem::subMenu('Categories', 'fas fa-bars')->setSubItems([
            MenuItem::linkToCrud('Créer une Catégorie', 'fas fa-plus-circle', ItemCategory::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Voir les catégories', 'fas fa-eye', ItemCategory::class),
        ]);
    }
}
