# Vinted-like DDD Architecture

## How to install

```bash
make up # Launch docker
make install # Install the project dependencies & database

make admin-user # Create an admin user
# make user is used to create a simple user

make dev # Build js & css files
```
